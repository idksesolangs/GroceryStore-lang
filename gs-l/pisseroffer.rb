class File
    def invalidgsl?
        i=1
        @atgrocery=false
        @validNames=['ginger', 'blueberry', 'popcorn', 'utensils', 'gingerBlueberry', 'gingerPopcorn', 'gingerUtensils', 'popcornBlueberry', 'popcornUtensils', 'utensilsBlueberry']
        self.each_line do |line|
            lines = line.split" "
            if lines[0].sub(":", '').downcase.deordinalize != i
                true
                fail "Invalid ordinal on line #{i}"
            end
            
            case lines[2]
            when "drove"
                if lines[5] == 'grocery'
                    @atgrocery=true
                    unless lines[6] == 'store'
                        true
                        fail "You tried to drive to an unknown location on line #{i}"
                    end
                    unless line.include? 'I drove to the grocery store'
                        true
                        fail "Invalid syntax on line #{i}"
                    end
                end
                if lines[3] == 'home'
                    unless line.include? 'I drove home'
                        true
                        fail "Invalid syntax on line #{i}"
                    end
                end
                if (lines[3]!='home') && (lines[5]!='grocery')
                    true
                    fail "You tried to drive to an unknown location on line #{i}"
                end
                if lines[7] 
                    true
                    fail "I don't even know what you messed up this time. Something on line #{i}"
                end
            when "bought"
                unless @atgrocery==true
                    true
                    fail "You tried to buy something while at home on line #{i}"
                end
                unless (line.include?'I bought') && (line.include?'that cost $')
                    true
                    fail "Invalid syntax on line #{i}"
                end
                unless @validNames.include?lines[3]
                    true
                    fail "Invalid variable name on line #{i}"
                end
                unless lines[-1].include?'...'
                    if lines[-1].gsub(/[$, !, '...'']/, '').to_i > 20
                    unless (lines[-1].include?'!') && (lines[-1].include?'$')
                        fail "Incorrect characters placed around a value on line #{i}" unless (lines[-1].include?'...') && (lines[-1].include?'$') 
                    end
                end
                if lines[-1].gsub(/[$, !, ...]/, '').to_i < 20
                    unless (lines[-1].include?'$')
                        true
                        fail "Incorrect characters placed around a value on line #{i}" 
                    end
                end
                end
                
            end
            
            i+=1
        end
    end
end
